#!/usr/bin/perl

use ExtUtils::testlib;
use libof;

my $ctl = libof::controller_new();
if (libof::controller_init($ctl, 6633, libof::of10_protocol())) {
	print "error initializing controller.\n";
} else {
	print "controller initialized successfully.\n";
}
libof::controller_loop($ctl);
