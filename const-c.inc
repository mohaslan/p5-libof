#define PERL_constant_NOTFOUND	1
#define PERL_constant_NOTDEF	2
#define PERL_constant_ISIV	3
#define PERL_constant_ISNO	4
#define PERL_constant_ISNV	5
#define PERL_constant_ISPV	6
#define PERL_constant_ISPVN	7
#define PERL_constant_ISSV	8
#define PERL_constant_ISUNDEF	9
#define PERL_constant_ISUV	10
#define PERL_constant_ISYES	11

#ifndef NVTYPE
typedef double NV; /* 5.6 and later define NVTYPE, and typedef NV to it.  */
#endif
#ifndef aTHX_
#define aTHX_ /* 5.6 or later define this for threading support.  */
#endif
#ifndef pTHX_
#define pTHX_ /* 5.6 or later define this for threading support.  */
#endif

static int
constant_17 (pTHX_ const char *name, IV *iv_return) {
  /* When generated this function returned values for the list of names given
     here.  However, subsequent manual editing may have added or removed some.
     OFPT10_ECHO_REPLY OFPT10_OFPT_ERROR OFPT10_PACKET_OUT OFPT10_SET_CONFIG */
  /* Offset 8 gives the best switch position.  */
  switch (name[8]) {
  case 'A':
    if (memEQ(name, "OFPT10_PACKET_OUT", 17)) {
    /*                       ^               */
#ifdef OFPT10_PACKET_OUT
      *iv_return = OFPT10_PACKET_OUT;
      return PERL_constant_ISIV;
#else
      return PERL_constant_NOTDEF;
#endif
    }
    break;
  case 'C':
    if (memEQ(name, "OFPT10_ECHO_REPLY", 17)) {
    /*                       ^               */
#ifdef OFPT10_ECHO_REPLY
      *iv_return = OFPT10_ECHO_REPLY;
      return PERL_constant_ISIV;
#else
      return PERL_constant_NOTDEF;
#endif
    }
    break;
  case 'E':
    if (memEQ(name, "OFPT10_SET_CONFIG", 17)) {
    /*                       ^               */
#ifdef OFPT10_SET_CONFIG
      *iv_return = OFPT10_SET_CONFIG;
      return PERL_constant_ISIV;
#else
      return PERL_constant_NOTDEF;
#endif
    }
    break;
  case 'F':
    if (memEQ(name, "OFPT10_OFPT_ERROR", 17)) {
    /*                       ^               */
#ifdef OFPT10_OFPT_ERROR
      *iv_return = OFPT10_OFPT_ERROR;
      return PERL_constant_ISIV;
#else
      return PERL_constant_NOTDEF;
#endif
    }
    break;
  }
  return PERL_constant_NOTFOUND;
}

static int
constant_23 (pTHX_ const char *name, IV *iv_return) {
  /* When generated this function returned values for the list of names given
     here.  However, subsequent manual editing may have added or removed some.
     OFP10_MAX_PORT_NAME_LEN OFPT10_FEATURES_REQUEST OFPT10_GET_CONFIG_REPLY */
  /* Offset 7 gives the best switch position.  */
  switch (name[7]) {
  case 'A':
    if (memEQ(name, "OFP10_MAX_PORT_NAME_LEN", 23)) {
    /*                      ^                      */
#ifdef OFP10_MAX_PORT_NAME_LEN
      *iv_return = OFP10_MAX_PORT_NAME_LEN;
      return PERL_constant_ISIV;
#else
      return PERL_constant_NOTDEF;
#endif
    }
    break;
  case 'F':
    if (memEQ(name, "OFPT10_FEATURES_REQUEST", 23)) {
    /*                      ^                      */
#ifdef OFPT10_FEATURES_REQUEST
      *iv_return = OFPT10_FEATURES_REQUEST;
      return PERL_constant_ISIV;
#else
      return PERL_constant_NOTDEF;
#endif
    }
    break;
  case 'G':
    if (memEQ(name, "OFPT10_GET_CONFIG_REPLY", 23)) {
    /*                      ^                      */
#ifdef OFPT10_GET_CONFIG_REPLY
      *iv_return = OFPT10_GET_CONFIG_REPLY;
      return PERL_constant_ISIV;
#else
      return PERL_constant_NOTDEF;
#endif
    }
    break;
  }
  return PERL_constant_NOTFOUND;
}

static int
constant (pTHX_ const char *name, STRLEN len, IV *iv_return) {
  /* Initially switch on the length of the name.  */
  /* When generated this function returned values for the list of names given
     in this section of perl code.  Rather than manually editing these functions
     to add or remove constants, which would result in this comment and section
     of code becoming inaccurate, we recommend that you edit this section of
     code, and use it to regenerate a new set of constant functions which you
     then use to replace the originals.

     Regenerate these constant functions by feeding this entire source file to
     perl -x

#!/usr/bin/perl -w
use ExtUtils::Constant qw (constant_types C_constant XS_constant);

my $types = {map {($_, 1)} qw(IV)};
my @names = (qw(OFP10_ETH_ALEN OFP10_MAX_PORT_NAME_LEN OFPT10_BARRIER_REPLY
	       OFPT10_BARRIER_REQUEST OFPT10_ECHO_REPLY OFPT10_ECHO_REQUEST
	       OFPT10_FEATURES_REPLY OFPT10_FEATURES_REQUEST OFPT10_FLOW_MOD
	       OFPT10_FLOW_REMOVED OFPT10_GET_CONFIG_REPLY
	       OFPT10_GET_CONFIG_REQUEST OFPT10_HELLO OFPT10_OFPT_ERROR
	       OFPT10_PACKET_IN OFPT10_PACKET_OUT OFPT10_PORT_MOD
	       OFPT10_PORT_STATUS OFPT10_QUEUE_GET_CONFIG_REPLY
	       OFPT10_QUEUE_GET_CONFIG_REQUEST OFPT10_SET_CONFIG
	       OFPT10_STATS_REPLY OFPT10_STATS_REQUEST OFPT10_VENDOR
	       OFP_VERSION_10));

print constant_types(), "\n"; # macro defs
foreach (C_constant ("libof", 'constant', 'IV', $types, undef, 3, @names) ) {
    print $_, "\n"; # C constant subs
}
print "\n#### XS Section:\n";
print XS_constant ("libof", $types);
__END__
   */

  switch (len) {
  case 12:
    if (memEQ(name, "OFPT10_HELLO", 12)) {
#ifdef OFPT10_HELLO
      *iv_return = OFPT10_HELLO;
      return PERL_constant_ISIV;
#else
      return PERL_constant_NOTDEF;
#endif
    }
    break;
  case 13:
    if (memEQ(name, "OFPT10_VENDOR", 13)) {
#ifdef OFPT10_VENDOR
      *iv_return = OFPT10_VENDOR;
      return PERL_constant_ISIV;
#else
      return PERL_constant_NOTDEF;
#endif
    }
    break;
  case 14:
    /* Names all of length 14.  */
    /* OFP10_ETH_ALEN OFP_VERSION_10 */
    /* Offset 7 gives the best switch position.  */
    switch (name[7]) {
    case 'S':
      if (memEQ(name, "OFP_VERSION_10", 14)) {
      /*                      ^             */
#ifdef OFP_VERSION_10
        *iv_return = OFP_VERSION_10;
        return PERL_constant_ISIV;
#else
        return PERL_constant_NOTDEF;
#endif
      }
      break;
    case 'T':
      if (memEQ(name, "OFP10_ETH_ALEN", 14)) {
      /*                      ^             */
#ifdef OFP10_ETH_ALEN
        *iv_return = OFP10_ETH_ALEN;
        return PERL_constant_ISIV;
#else
        return PERL_constant_NOTDEF;
#endif
      }
      break;
    }
    break;
  case 15:
    /* Names all of length 15.  */
    /* OFPT10_FLOW_MOD OFPT10_PORT_MOD */
    /* Offset 8 gives the best switch position.  */
    switch (name[8]) {
    case 'L':
      if (memEQ(name, "OFPT10_FLOW_MOD", 15)) {
      /*                       ^             */
#ifdef OFPT10_FLOW_MOD
        *iv_return = OFPT10_FLOW_MOD;
        return PERL_constant_ISIV;
#else
        return PERL_constant_NOTDEF;
#endif
      }
      break;
    case 'O':
      if (memEQ(name, "OFPT10_PORT_MOD", 15)) {
      /*                       ^             */
#ifdef OFPT10_PORT_MOD
        *iv_return = OFPT10_PORT_MOD;
        return PERL_constant_ISIV;
#else
        return PERL_constant_NOTDEF;
#endif
      }
      break;
    }
    break;
  case 16:
    if (memEQ(name, "OFPT10_PACKET_IN", 16)) {
#ifdef OFPT10_PACKET_IN
      *iv_return = OFPT10_PACKET_IN;
      return PERL_constant_ISIV;
#else
      return PERL_constant_NOTDEF;
#endif
    }
    break;
  case 17:
    return constant_17 (aTHX_ name, iv_return);
    break;
  case 18:
    /* Names all of length 18.  */
    /* OFPT10_PORT_STATUS OFPT10_STATS_REPLY */
    /* Offset 13 gives the best switch position.  */
    switch (name[13]) {
    case 'R':
      if (memEQ(name, "OFPT10_STATS_REPLY", 18)) {
      /*                            ^           */
#ifdef OFPT10_STATS_REPLY
        *iv_return = OFPT10_STATS_REPLY;
        return PERL_constant_ISIV;
#else
        return PERL_constant_NOTDEF;
#endif
      }
      break;
    case 'T':
      if (memEQ(name, "OFPT10_PORT_STATUS", 18)) {
      /*                            ^           */
#ifdef OFPT10_PORT_STATUS
        *iv_return = OFPT10_PORT_STATUS;
        return PERL_constant_ISIV;
#else
        return PERL_constant_NOTDEF;
#endif
      }
      break;
    }
    break;
  case 19:
    /* Names all of length 19.  */
    /* OFPT10_ECHO_REQUEST OFPT10_FLOW_REMOVED */
    /* Offset 7 gives the best switch position.  */
    switch (name[7]) {
    case 'E':
      if (memEQ(name, "OFPT10_ECHO_REQUEST", 19)) {
      /*                      ^                  */
#ifdef OFPT10_ECHO_REQUEST
        *iv_return = OFPT10_ECHO_REQUEST;
        return PERL_constant_ISIV;
#else
        return PERL_constant_NOTDEF;
#endif
      }
      break;
    case 'F':
      if (memEQ(name, "OFPT10_FLOW_REMOVED", 19)) {
      /*                      ^                  */
#ifdef OFPT10_FLOW_REMOVED
        *iv_return = OFPT10_FLOW_REMOVED;
        return PERL_constant_ISIV;
#else
        return PERL_constant_NOTDEF;
#endif
      }
      break;
    }
    break;
  case 20:
    /* Names all of length 20.  */
    /* OFPT10_BARRIER_REPLY OFPT10_STATS_REQUEST */
    /* Offset 15 gives the best switch position.  */
    switch (name[15]) {
    case 'Q':
      if (memEQ(name, "OFPT10_STATS_REQUEST", 20)) {
      /*                              ^           */
#ifdef OFPT10_STATS_REQUEST
        *iv_return = OFPT10_STATS_REQUEST;
        return PERL_constant_ISIV;
#else
        return PERL_constant_NOTDEF;
#endif
      }
      break;
    case 'R':
      if (memEQ(name, "OFPT10_BARRIER_REPLY", 20)) {
      /*                              ^           */
#ifdef OFPT10_BARRIER_REPLY
        *iv_return = OFPT10_BARRIER_REPLY;
        return PERL_constant_ISIV;
#else
        return PERL_constant_NOTDEF;
#endif
      }
      break;
    }
    break;
  case 21:
    if (memEQ(name, "OFPT10_FEATURES_REPLY", 21)) {
#ifdef OFPT10_FEATURES_REPLY
      *iv_return = OFPT10_FEATURES_REPLY;
      return PERL_constant_ISIV;
#else
      return PERL_constant_NOTDEF;
#endif
    }
    break;
  case 22:
    if (memEQ(name, "OFPT10_BARRIER_REQUEST", 22)) {
#ifdef OFPT10_BARRIER_REQUEST
      *iv_return = OFPT10_BARRIER_REQUEST;
      return PERL_constant_ISIV;
#else
      return PERL_constant_NOTDEF;
#endif
    }
    break;
  case 23:
    return constant_23 (aTHX_ name, iv_return);
    break;
  case 25:
    if (memEQ(name, "OFPT10_GET_CONFIG_REQUEST", 25)) {
#ifdef OFPT10_GET_CONFIG_REQUEST
      *iv_return = OFPT10_GET_CONFIG_REQUEST;
      return PERL_constant_ISIV;
#else
      return PERL_constant_NOTDEF;
#endif
    }
    break;
  case 29:
    if (memEQ(name, "OFPT10_QUEUE_GET_CONFIG_REPLY", 29)) {
#ifdef OFPT10_QUEUE_GET_CONFIG_REPLY
      *iv_return = OFPT10_QUEUE_GET_CONFIG_REPLY;
      return PERL_constant_ISIV;
#else
      return PERL_constant_NOTDEF;
#endif
    }
    break;
  case 31:
    if (memEQ(name, "OFPT10_QUEUE_GET_CONFIG_REQUEST", 31)) {
#ifdef OFPT10_QUEUE_GET_CONFIG_REQUEST
      *iv_return = OFPT10_QUEUE_GET_CONFIG_REQUEST;
      return PERL_constant_ISIV;
#else
      return PERL_constant_NOTDEF;
#endif
    }
    break;
  }
  return PERL_constant_NOTFOUND;
}

