#define PERL_NO_GET_CONTEXT
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "ppport.h"

#include <libof.h>
#include <of10.h>

#include "const-c.inc"

typedef struct of_protocol* protocol;
typedef struct of_controller* controller;

MODULE = libof		PACKAGE = libof		

INCLUDE: const-xs.inc

TYPEMAP: <<END
	protocol T_PTROBJ
	controller T_PTROBJ
END

protocol
of10_protocol()
	CODE:
		RETVAL = of10_protocol();
	OUTPUT:
		RETVAL

controller
controller_new()
	CODE:
		RETVAL = malloc(sizeof(controller));
	OUTPUT:
		RETVAL

int
controller_init(ctl, port, proto)
		controller ctl
		int port
		protocol proto
	CODE:
		RETVAL = of_controller_init(ctl, port, proto);
	OUTPUT:
		RETVAL

void
controller_loop(ctl)
		controller ctl
	CODE:
		ctl->loop(ctl);

